#include "utils.h"
#include "snake_utils.h"
#include "menu.h"
#include <stdio.h>
#include <pthread.h>

menu_option_t game_over;

size_t strlen(const char* string)
{
    size_t lenght = 0;
    while (*string != '\0')
    {
        lenght++;
        string++;
    }    
    return lenght;
}

void* blink_left_led_blue(void* vargp)
{
    uint8_t *spiled_reg_base = (uint8_t*)vargp;
    knobs_color_t prev = *((knobs_color_t*)(spiled_reg_base + SPILED_REG_LED_RGB1_o));
    knobs_color_t color = {.blue = 255};
    *((knobs_color_t*)(spiled_reg_base + SPILED_REG_LED_RGB1_o)) = color;
    parlcd_delay(100);
    *((knobs_color_t*)(spiled_reg_base + SPILED_REG_LED_RGB1_o)) = prev;
    pthread_exit(0);
    return NULL;
}

void* blink_right_led_blue(void* vargp)
{
    uint8_t *spiled_reg_base = (uint8_t*)vargp;
    knobs_color_t prev = *((knobs_color_t*)(spiled_reg_base + SPILED_REG_LED_RGB2_o));
    knobs_color_t color = {.blue = 255};
    *((knobs_color_t*)(spiled_reg_base + SPILED_REG_LED_RGB2_o)) = color;
    parlcd_delay(100);
    *((knobs_color_t*)(spiled_reg_base + SPILED_REG_LED_RGB2_o)) = prev;
    pthread_exit(0);
    return NULL;
}

uint32_t string_lenght(const char *string, font_descriptor_t font, uint8_t scaling)
{
    uint32_t lenght = 0;
    if (font.width != 0)
    {
        while (*string != '\0')
        {
            lenght += font.width[(*string) - font.firstchar];
            string++;
        }
    }
    else
    {
        lenght = strlen(string) * font.maxwidth;
    }
    return lenght * scaling;
}

void get_565_color_from_knobs(color_t *color_565, volatile uint8_t *spiled_reg_base)
{
    volatile knobs_color_t *spiled_knobs = (knobs_color_t*)(spiled_reg_base + SPILED_REG_KNOBS_8BIT_o);
    uint8_t red = spiled_knobs->red;
    uint8_t green = spiled_knobs->green;
    uint8_t blue = spiled_knobs->blue;

    color_565->_rgb.red = red>>3;
    color_565->_rgb.green = green>>2;
    color_565->_rgb.blue = blue>>3;
}

void write_to_led(io_t *io, uint32_t value)
{
    *(volatile uint32_t*)(io->spiled_reg_base + SPILED_REG_LED_LINE_o) = value;
}

void write_knobs_to_leds(volatile uint8_t *spiled_reg_base)
{
    volatile knobs_color_t *spiled_knobs = (knobs_color_t*)(spiled_reg_base + SPILED_REG_KNOBS_8BIT_o);
    volatile knobs_color_t *spiled_led1 = (knobs_color_t*)(spiled_reg_base + SPILED_REG_LED_RGB1_o);
    volatile knobs_color_t *spiled_led2 = (knobs_color_t*)(spiled_reg_base + SPILED_REG_LED_RGB2_o);
    *spiled_led1 = *spiled_knobs;
    *spiled_led2 = *spiled_knobs;
}

void write_to_left_led(volatile uint8_t *spiled_reg_base, knobs_color_t color)
{
    *((knobs_color_t*)(spiled_reg_base + SPILED_REG_LED_RGB1_o)) = color;
}

void write_to_right_led(volatile uint8_t *spiled_reg_base, knobs_color_t color)
{
    *((knobs_color_t*)(spiled_reg_base + SPILED_REG_LED_RGB2_o)) = color;
}

void init_lcd(color_t initial_color, io_t *io)
{    
    io->parlcd_reg_base = (volatile uint8_t*)map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);

    parlcd_hx8357_init((unsigned char *)io->parlcd_reg_base);

    io->lcd.height = HEIGHT;
    io->lcd.width = WIDTH;
    io->lcd.size = HEIGHT * WIDTH;
    io->lcd.lcd_data_start = (color_t*)(io->parlcd_reg_base + PARLCD_REG_DATA_o);
    io->lcd.lcd_cmd_start = (uint16_t*)(io->parlcd_reg_base + PARLCD_REG_CMD_o);

    io->lcd.lcd = (color_t*)malloc(io->lcd.size * 2);

    *(io->lcd.lcd_cmd_start) = 0x2c;
    for (uint32_t i = 0; i < io->lcd.size; i++)
    {
        *(io->lcd.lcd_data_start) = initial_color;
        *(io->lcd.lcd + 1) = initial_color;
    }
    parlcd_delay(5000);
}

void print_lcd(lcd_t *lcd)
{
    *(lcd->lcd_cmd_start) = 0x2c;
    for (uint32_t i = 0; i < lcd->size; i++)
    {
        *(lcd->lcd_data_start) = *(lcd->lcd + i);
    }
}

void init_io(color_t initial_color, io_t *io)
{
    init_lcd(initial_color, io);
    io->spiled_reg_base = 
    (uint8_t*)map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);

    knobs_color_t color = {.red = 0, .green = 0, .blue = 0};
    write_to_left_led(io->spiled_reg_base, color);
    write_to_right_led(io->spiled_reg_base, color);
}

void read_char(char *c)
{
    if (system("/bin/stty raw") == -1)
    {
        exit(100);
    }    
    *c = getchar();
    if (system("/bin/stty cooked") == -1)
    {
        exit(100);
    }  
}

void process_terminal_input(game_t *game, bool *menu_open, bool *run, font_descriptor_t font)
{
    char user_input = 'e';    
    read_char(&user_input);

    if (!(*run))
    {
        return;
    }
    

    write_knobs_to_leds(game->io.spiled_reg_base);
    switch (user_input)
    {
    case 'e':
    case 'E':        
        if (*menu_open)
        {
            process_selected_option(menu_open, run, font, game);               
        }
        else
        {
            *menu_open = true;
            delete_snake(&(game->snake1), game);
            delete_snake(&(game->snake2), game);
            print_menu(font);
        }
        break;
    case 'w':
    case 'W':
        if (*menu_open)
        {
            select_option_above(font);                
        }
        else
        {
            //Set snake1 direction up
            if(game->snake1.direction != DOWN) game->snake1.direction = UP;
        }
        break;
    case 's':
    case 'S':
        if (*menu_open)
        {
            select_option_below(font);            
        }
        else
        {
            //Set snake1 direction left
            if(game->snake1.direction != UP) game->snake1.direction = DOWN;
        }            
        break;
    case 'a':
    case 'A':
        if (!(*menu_open))
        {
            //Set snake1 direction left    
            if(game->snake1.direction != RIGHT) game->snake1.direction = LEFT;             
        }       
        break;  
    case 'd':
    case 'D':
        if (!(*menu_open))
        {
            //Set snake1 direction right 
            if(game->snake1.direction != LEFT) game->snake1.direction = RIGHT;             
        }
        break;
    case 'i':
    case 'I':
        if (*menu_open)
        {
            select_option_above(font);                
        }
        else
        {
            //Set snake2 direction up   
            if(game->snake2.direction != DOWN) game->snake2.direction = UP;
        }            
        break;  
    case 'k':
    case 'K':
        if (*menu_open)
        {
            select_option_below(font);            
        }
        else
        {
            //Set snake2 direction left
            if(game->snake2.direction != UP) game->snake2.direction = DOWN;
        }            
        break;
    case 'j':
    case 'J':
        if (!(*menu_open))
        {
            //Set snake2 direction left   
            if(game->snake2.direction != RIGHT)game->snake2.direction = LEFT;               
        }       
        break;  
    case 'l':
    case 'L':
        if (!(*menu_open))
        {
            //Set snake2 direction right  
            if(game->snake2.direction != LEFT)game->snake2.direction = RIGHT;            
        }
        break;     
    default:
        break;
    }
}

int strcmp(const char* text1, const char* text2)
{
    while (*text1 != '\0' || *text2 != '\0')
    {
        if(*text1 != *text2) return -1;
        text1++;
        text2++;
    }
    return 0;
}

void init_game(color_t initial_color, game_t *game)
{
    init_io(initial_color, &(game->io));
    game->game_speed = 1;
}

void init_start(game_t* game)
{
    point_t pos1;
    pos1.x = (INIT_SNAKE_LENGHT + 1) * SNAKE_SCALE;
    pos1.y = (INIT_SNAKE_LENGHT + 1) * SNAKE_SCALE;

    point_t pos2;
    pos2.x = WIDTH - (INIT_SNAKE_LENGHT + 1) * SNAKE_SCALE;
    pos2.y = HEIGHT - (INIT_SNAKE_LENGHT + 1) * SNAKE_SCALE;    

    knobs_color_t color = { .red = 0, .green = 255, .blue = 0};
    write_to_left_led(game->io.spiled_reg_base, color);
    write_to_right_led(game->io.spiled_reg_base, color);

    init_snake(&(game->snake1), pos1, INIT_SNAKE_LENGHT, UP);
    init_snake(&(game->snake2), pos2, INIT_SNAKE_LENGHT, DOWN);

    draw_snake_rectangle(game, game->snake1.head, game->snake1.head_color);
    for(int i = 0; i <  game->snake1.body_lenght; i++)
    {
        draw_snake_rectangle(game, game->snake1.body[i], game->snake1.body_color);
    }
    draw_snake_rectangle(game, game->snake2.head, game->snake2.head_color);
    for(int i = 0; i <  game->snake2.body_lenght; i++)
    {
        draw_snake_rectangle(game, game->snake2.body[i], game->snake2.body_color);
    }

    generate_food(game);
}

bool check_win_conditions(game_t* game, font_descriptor_t font, int* win_score)
{
	bool end = false;
	if(game->snake1.head.x == game->snake2.head.x && game->snake1.head.y == game->snake2.head.y)
	{
		if(game->snake1.body_lenght > game->snake2.body_lenght) print_text_to_serp("Snake 1 wins the game!\n");
		else if(game->snake2.body_lenght > game->snake1.body_lenght) print_text_to_serp("Snake 2 wins the game!\n");
		else if(game->snake2.body_lenght == game->snake1.body_lenght) print_text_to_serp("Game ended in a draw!\n");
		end_game(game, font);
		end = true;
	}
	else if(game->snake1.body_lenght >= *win_score)
	{
		print_text_to_serp("Snake 1 wins the game!\n");
		end_game(game, font);
		end = true;
	}
	else if(game->snake2.body_lenght >= *win_score)
	{
		print_text_to_serp("Snake 2 wins the game!\n");
		end_game(game, font);
		end = true;
	}
	else if(game->snake2.stopped && game->snake1.stopped && game->snake2.body_lenght == game->snake1.body_lenght)
	{
		print_text_to_serp("Game ended in a draw!\n");
		end_game(game, font);
		end = true;
	}	
	
	if(!game->snake1.stopped && snake_collision(&(game->snake1), game->snake2, game))
	{
	    *win_score = game->snake1.body_lenght + 1;
	}
		
	if(!game->snake2.stopped && snake_collision(&(game->snake2), game->snake1, game))
	{
			*win_score = game->snake2.body_lenght + 1;
	} 
   
    return end;
}

void print_text_to_serp(char *text)
{
    if (system("/bin/stty cooked") == -1)
    {
        exit(100);
    }     
    putchar('\n');
    printf("%s", text);  
}

void end_game(game_t *game, font_descriptor_t font)
{
    color_t red = RED;
    color_t black = BLACK;

    for (int i = 0; i < game->io.lcd.size; i++)
    {
        game->io.lcd.lcd[i] = black;
    }
    
    volatile color_t *start = game->io.lcd.lcd + (HEIGHT - font.height * 5)/2
    * WIDTH + (WIDTH - (string_lenght("GAME OVER!", font, 5)))/2;
    print_string(start, font, "GAME OVER!", red, black, 5);
}
