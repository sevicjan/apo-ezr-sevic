#ifndef COLOR_H
#define COLOR_H

#include <stdint.h>

typedef struct __attribute__((packed))
{
    unsigned blue : 5;
    unsigned green : 6;
    unsigned red : 5;
} rgb;

typedef union __attribute__((packed))
{
    rgb _rgb;
    uint16_t _value;
} color_t;

typedef struct __attribute__((packed))
{
    uint8_t blue;
    uint8_t green;
    uint8_t red;
    uint8_t empty;
} knobs_color_t;

#define WHITE { ._value = 0xffff}
#define RED { ._rgb = { .red = 0b11111, .green = 0b0, .blue = 0b0}}
#define BLUE { ._rgb = { .red = 0b0, .green = 0b0, .blue = 0b11111}}
#define DARK_BLUE { ._rgb = { .red = 0b0, .green = 0b0, .blue = 0b10000}}
#define GREEN { ._rgb = { .red = 0b0, .green = 0b111111, .blue = 0b0}}
#define BLACK { ._value = 0x0}

#endif