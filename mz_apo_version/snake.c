/*
Authors: Frantisek Ezr, Sevic Jan
License: FreeBSD License
*/

#define _POSIX_C_SOURCE 200112L

#include "snake_utils.h"
#include "snake_ai.h"
#include "menu.h"
#include "font_rom8x16.c"
#include "font_prop14x16.c"
#include <pthread.h>

game_t game;
bool run = true;
bool menu_open = true;

int main(int argc, char ** argv)
{
    color_t black = BLACK;

    init_game(black, &game);
    init_menu(game.io.lcd.lcd, font_winFreeSystem14x16, &(game.snake1), &(game.snake2), 4);

    load_animation();

    print_menu(font_winFreeSystem14x16);
    print_lcd(&(game.io.lcd));

    pthread_t thread_id;
    pthread_create(&thread_id, NULL, input_thread, NULL);
    
    int win_score = WIN_SCORE;

    while (run)
    {
        if(!menu_open) 
        {
            if(game.snake1.ai && !game.snake1.stopped) set_snake_direction(&(game.snake1), game.snake2, &game);
            if(game.snake2.ai && !game.snake2.stopped) set_snake_direction(&(game.snake2), game.snake1, &game);
            update_snakes(&game);
            run = !check_win_conditions(&game, font_winFreeSystem14x16, &win_score);
            if (game.snake1.stopped)
            {
                knobs_color_t color = {.red = 255};
                write_to_left_led(game.io.spiled_reg_base, color);
            }
            if (game.snake2.stopped)
            {
                knobs_color_t color = {.red = 255};
                write_to_right_led(game.io.spiled_reg_base, color);
            }            
        }
        print_lcd(&(game.io.lcd));
        parlcd_delay(10 / game.game_speed);
    }
    
    free(game.io.lcd.lcd);
    if (system("/bin/stty cooked") == -1)
    {
        exit(100);
    }     
    knobs_color_t color = {.red = 0, .green = 0, .blue = 0};
    write_to_left_led(game.io.spiled_reg_base, color);
    write_to_right_led(game.io.spiled_reg_base, color);
    return 0;
}

void *input_thread(void *vargp)
{
    while (run)
    {
        process_terminal_input(&game, &menu_open, &run, font_winFreeSystem14x16);
    }
    return NULL;
}

void load_animation()
{
    for (int i = 0; i < 32; i++)
    {
        write_to_led(&(game.io), 1<<i);
        parlcd_delay(50);
    }    

    for (int i = 0; i < 32; i++)
    {
        write_to_led(&(game.io), ~(0xffffffff>>i));
        parlcd_delay(50);
    }    

    write_to_led(&(game.io), 0xffffffff);
    parlcd_delay(500);
    write_to_led(&(game.io), 0);
}
