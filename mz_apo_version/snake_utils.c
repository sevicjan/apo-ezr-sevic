#include "snake_utils.h"
#include "utils.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

void delete_snake(snake_t* snake, game_t* game)
{
	color_t black = BLACK;
	for(uint16_t i = 0; i < snake->body_lenght; i++)
	{
        draw_snake_rectangle(game, snake->body[i], black);
	}
}

void generate_food(game_t* game)
{
    point_t food;
    bool loop;
    time_t t;
    color_t green = GREEN;
    srand((unsigned) time(&t));

    while(true)
    {
        loop = false;
        food.x = (rand() % (WIDTH / SNAKE_SCALE)) * SNAKE_SCALE;
        food.y = (rand() % (HEIGHT / SNAKE_SCALE)) * SNAKE_SCALE;
             
        if(!game->snake1.stopped)
        {
        	if(food.x == game->snake1.head.x && food.y ==  game->snake1.head.y) continue;
        	for(int i = 0; i <  game->snake1.body_lenght; i++)
		    {
		        if( game->snake1.body[i].x == food.x &&  game->snake1.body[i].y == food.y)
		        {
		            loop = true;
		            break;
		        }
		    }
        }
        
        if(!game->snake2.stopped)
        {
        	if(food.x ==  game->snake2.head.x && food.y ==  game->snake2.head.y)  continue;
			for(int i = 0; i <  game->snake2.body_lenght; i++)
		    {
		        if( game->snake2.body[i].x == food.x &&  game->snake2.body[i].y == food.y)
		        {
		            loop = true;
		            break;
		        }
		    } 
        }        
        if(loop) continue;
        break;
    }

    game->food.x = food.x;
    game->food.y = food.y;
    draw_snake_rectangle(game, game->food, green);
}

void init_snake(snake_t* snake, point_t head_pos, uint16_t b_lenght, int direction)
{
    snake->head = head_pos;
    snake->body_lenght = b_lenght;
    snake->stopped = false;
    snake->ai = true;     
    if(direction == UP) 
    {
        snake->direction = DOWN;
        for(int i = 1; i <= b_lenght; i++)
        {
            snake->body[i-1].x = snake->head.x; 
            snake->body[i-1].y = snake->head.y - (i * SNAKE_SCALE);
        }
    }      
    else if(direction == DOWN)
    {
        snake->direction = UP;
        for(int i = 1; i <= b_lenght; i++)
        {
            snake->body[i-1].x = snake->head.x; 
            snake->body[i-1].y = snake->head.y + (i * SNAKE_SCALE);
        }
    }
    else if(direction == LEFT)
    {
        snake->direction = RIGHT;
        for(int i = 1; i <= b_lenght; i++)
        {
            snake->body[i-1].x = snake->head.x - (i * SNAKE_SCALE); 
            snake->body[i-1].y = snake->head.y;
        }
    }
    else if(direction == RIGHT)
    {
        snake->direction = LEFT;
        for(int i = 1; i <= b_lenght; i++)
        {
            snake->body[i-1].x = snake->head.x + (i * SNAKE_SCALE); 
            snake->body[i-1].y = snake->head.y;
        }
    }
}

bool food_eaten(snake_t* snake, point_t food)
{
    if(snake->head.x == food.x && snake->head.y == food.y)
    {
        snake->body_lenght += 1;
        return true;
    }
    else return false;
}

void move_snake(snake_t* snake)
{   
    point_t body_buffer[2];
    body_buffer[0].x = snake->head.x;
    body_buffer[0].y = snake->head.y;

    for(uint16_t i = 0; i < snake->body_lenght-1; i++)
    {
        body_buffer[(i + 1) & 1].x = snake->body[i].x;
        body_buffer[(i + 1) & 1].y = snake->body[i].y;

        snake->body[i].x = body_buffer[i & 1].x;
        snake->body[i].y = body_buffer[i&1].y;
    }
    snake->body[snake->body_lenght - 1].x = body_buffer[(snake->body_lenght - 1) & 1].x;
    snake->body[snake->body_lenght - 1].y = body_buffer[(snake->body_lenght - 1) & 1].y;

    if(snake->direction == UP) snake->head.y -= SNAKE_SCALE;          
    else if(snake->direction == DOWN) snake->head.y += SNAKE_SCALE;
    else if(snake->direction == LEFT) snake->head.x -= SNAKE_SCALE;
    else if(snake->direction == RIGHT) snake->head.x += SNAKE_SCALE;   
}

void update_snakes(game_t* game)
{
    color_t black = BLACK;
    if(!game->snake1.stopped)
    {    
        if(!food_eaten(&(game->snake1), game->food))
        {
            draw_snake_rectangle(game, game->snake1.body[game->snake1.body_lenght-1], black);
        }
        move_snake(&(game->snake1));
        if (food_eaten(&(game->snake1), game->food))
        {            
            pthread_t thread_id;
            pthread_create(&thread_id, NULL, blink_left_led_blue, (void*)game->io.spiled_reg_base);
            generate_food(game);
        }        
        draw_snake_rectangle(game, game->snake1.head, game->snake1.head_color);
        draw_snake_rectangle(game, game->snake1.body[0], game->snake1.body_color);
    }
    
    if(!game->snake2.stopped)
    {
        if(!food_eaten(&(game->snake2), game->food))
        {
            draw_snake_rectangle(game, game->snake2.body[game->snake2.body_lenght-1], black);
        }
        move_snake(&(game->snake2));
        if (food_eaten(&(game->snake2), game->food))
        {            
            pthread_t thread_id;
            pthread_create(&thread_id, NULL, blink_right_led_blue, (void*)game->io.spiled_reg_base);
            generate_food(game);
        }
        draw_snake_rectangle(game, game->snake2.head, game->snake2.head_color); 
        draw_snake_rectangle(game, game->snake2.body[0], game->snake2.body_color);
    }
}

void draw_snake_rectangle(game_t* game, point_t position, color_t color)
{
    if (snake_in_lcd_bounds(position))
    {
        for(uint16_t i = 0; i < SNAKE_SCALE; i++)
        {
            for(uint16_t j = 0; j < SNAKE_SCALE; j++)
            {
                game->io.lcd.lcd[(position.y + j) * WIDTH + (position.x + i)] = color;
            }
        }   
    }    
}

bool snake_collision(snake_t *snake, snake_t op_snake, game_t* game)
{
	bool collided = false;
    if(snake->head.x >= WIDTH || snake->head.x < 0 || snake->head.y >= HEIGHT || snake->head.y < 0)
    {
        collided = true;
    }
    for(int i = 0; i < snake->body_lenght; i++)
    {
        if(snake->head.x == snake->body[i].x && snake->head.y == snake->body[i].y)
        {
        	collided = true;
        }
    }
    if(!op_snake.stopped)
    {
    	if(snake->head.x == op_snake.head.x && snake->head.y == op_snake.head.y)
		{
			collided = true;
		}
		for(int i = 0; i < op_snake.body_lenght; i++)
		{
		    if(snake->head.x == op_snake.body[i].x && snake->head.y == op_snake.body[i].y)
		    {
		    	collided = true;
		    }
		}
    }
    
    if(collided)
    {
        delete_snake(snake, game);
        snake->stopped = true;
    }
    return collided;
}

bool snake_in_lcd_bounds(point_t head)
{
    if(head.x < WIDTH && head.x >= 0 && head.y < HEIGHT && head.y >= 0) return true;
    else return false;
}
