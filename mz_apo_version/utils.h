#ifndef UTILS_H
#define UTILS_H

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "color.h"
#include "mzapo_regs.h"
#include "mzapo_phys.h"
#include "mzapo_parlcd.h"
#include "font_types.h"
#include "snake.h"

#define HEIGHT 320
#define WIDTH 480

typedef struct rectangle
{
    volatile color_t *lcd_rect_start;
    color_t color;
    uint16_t width;
    uint16_t height;
} rectangle_t;

typedef struct
{
    uint16_t height;
    uint16_t width;
    uint32_t size;
    volatile color_t *lcd_data_start;
    volatile uint16_t *lcd_cmd_start;
    color_t *lcd;
} lcd_t;

typedef struct
{
    lcd_t lcd;
    volatile uint8_t *parlcd_reg_base;
    volatile uint8_t *spiled_reg_base;
} io_t;

typedef struct
{
    io_t io;
    uint8_t game_speed;
    snake_t snake1;
    snake_t snake2;
    point_t food;
} game_t;

void write_to_led(io_t *io, uint32_t value);

void get_565_color_from_knobs(color_t *color_565, volatile uint8_t *spiled_reg_base);

void write_knobs_to_leds(volatile uint8_t *spiled_reg_base);

void init_lcd(color_t initial_color, io_t *io);

void print_lcd(lcd_t *lcd);

void* blink_left_led_blue(void* vargp);

void* blink_right_led_blue(void* vargp);

void write_to_left_led(volatile uint8_t *spiled_reg_base, knobs_color_t color);

void write_to_right_led(volatile uint8_t *spiled_reg_base, knobs_color_t color);

void init_io(color_t initial_color, io_t *io);

void read_char(char *c);

void* blink_leds(void* vargp);

void read_char_stop_and_wait(volatile uint8_t *serial_port_base, char *c);

void process_terminal_input(game_t *game, bool *menu_open, bool *run,
font_descriptor_t font);

void init_game(color_t initial_color, game_t *game);

int strcmp(const char* text1, const char* text2);

void init_start(game_t* game);

void print_text_to_serp(char *text);

bool check_win_conditions(game_t* game, font_descriptor_t font,  int* win_score);

uint32_t string_lenght(const char *string, font_descriptor_t font, uint8_t scaling);

void end_game(game_t *game, font_descriptor_t font);

#endif
