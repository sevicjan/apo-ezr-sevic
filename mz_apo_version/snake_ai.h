#ifndef SNAKE_AI
#define SNAKE_AI

#include "snake_utils.h"
#include "utils.h"
#include <stdlib.h>
#include <stdbool.h>

#define OBSTACLE -2
#define FREE_TILE -1
#define NO_MOVE_FOUND -100

point_t find_next_move(snake_t snake1, snake_t snake2, game_t* game);

point_t bactrack(int** food_path, game_t* game, point_t position);

void set_neighbours(int** food_path, uint16_t i, uint16_t j);

void set_snake_direction(snake_t* snake1, snake_t snake2, game_t* game);

point_t get_max_node(int** array);

#endif
