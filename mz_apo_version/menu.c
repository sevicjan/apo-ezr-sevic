#include "menu.h"
#include "snake_utils.h"

menu_t menu;

void print_char(volatile color_t *lcd_char_start, font_descriptor_t font,
char c, color_t text_fill, color_t background_fill, uint8_t scaling)
{
    int height_offset = 0; 
    for (int i = 0; i < font.height; i++)
    {
        uint16_t value = font.bits[(c - font.firstchar) * font.height + i];
        int width;
        if (font.width != 0)
        {
            width = font.width[c - font.firstchar];
        }
        else
        {
            width = font.maxwidth;
        }
        int width_stop = 16 - width;     
        
        for (int h_scale = 0; h_scale < scaling; h_scale++)
        {
            int width_offset = 0;
            for (int j = 15; j >= width_stop; j--)
            {
                for (int w_scale = 0; w_scale < scaling; w_scale++)
                {
                    if ((value>>j)&0b1)
                    {
                        *(lcd_char_start + height_offset + width_offset) = text_fill;
                    }
                    else
                    {
                        *(lcd_char_start + height_offset + width_offset) = background_fill;
                    }
                    width_offset++;
                }
            }
            height_offset += WIDTH;
        }
    }
}

void print_string(volatile color_t *lcd_string_start, font_descriptor_t font,
char* string, color_t text_fill, color_t background_fill, uint8_t scaling)
{
    int row_offset = 0;
    int char_offset = 0;
    while (*string != '\0')
    {
        print_char(lcd_string_start + row_offset + char_offset, font,
        *string, text_fill, background_fill, scaling);

        if (font.width != 0)
        {
            char_offset += font.width[(*string) - font.firstchar] * scaling;
        }
        else
        {
            char_offset += font.maxwidth * scaling;
        }

        if (char_offset > WIDTH - font.maxwidth * scaling)
        {
            char_offset = 0;
            row_offset += WIDTH * font.height * scaling;
        }
        string++;
    }
}

void init_menu(volatile color_t* lcd_start, font_descriptor_t font, 
snake_t *snake1, snake_t *snake2, uint8_t scaling)
{
    color_t black = BLACK;
    color_t red = RED;
    color_t blue = BLUE;
    color_t dark_blue = DARK_BLUE;

    menu.lenght = 4;
    menu.selected = 0;
    menu.scaling = scaling;
    menu.menu_id = 0;

    volatile color_t *lcd_resume_start = lcd_start + (HEIGHT - menu.lenght * font.height * scaling)/2
    * WIDTH + (WIDTH - (string_lenght(OP_S2C_TEXT, font, scaling) + 24))/2;

    init_option(menu.menu[0], lcd_resume_start, OP_S_TEXT, font, scaling, red, dark_blue, black, black);

    volatile color_t *lcd_s1c_start = lcd_resume_start + font.height * WIDTH * scaling;
    
    snake1->head_color = blue;
    snake1->body_color = blue;

    init_option(menu.menu[0] + 1, lcd_s1c_start, OP_S1C_TEXT, font, scaling, red, black, black, blue);
 
    volatile color_t *lcd_s2c_start = lcd_s1c_start + font.height * WIDTH * scaling;

    snake2->head_color = red;
    snake2->body_color = red;

    init_option(menu.menu[0] + 2, lcd_s2c_start, OP_S2C_TEXT, font, scaling, red, black, black, red);

    volatile color_t *lcd_exit_start = lcd_s2c_start + font.height * WIDTH * scaling;

    init_option(menu.menu[0] + 3, lcd_exit_start, OP_E_TEXT, font, scaling, red, black, black, black);

    init_option(menu.menu[1], lcd_resume_start, OP_PP_TEXT, font, scaling, red, dark_blue, black, black);

    init_option(menu.menu[1] + 1, lcd_s1c_start, OP_PA_TEXT, font, scaling, red, black, black, black); 

    init_option(menu.menu[1] + 2, lcd_s2c_start, OP_AA_TEXT, font, scaling, red, black, black, black);

    init_option(menu.menu[1] + 3, lcd_exit_start, OP_E_TEXT, font, scaling, red, black, black, black);

    menu.menu[0][0].rectangle.width = 0;
    menu.menu[0][0].rectangle.height = 0;
    menu.menu[0][0].border.width = 0;
    menu.menu[0][0].border.height = 0;
    menu.menu[0][3].rectangle.width = 0;
    menu.menu[0][3].rectangle.height = 0;
    menu.menu[0][3].border.width = 0;
    menu.menu[0][3].border.height = 0;
    for (int i = 0; i < 4; i++)
    {
        menu.menu[1][i].rectangle.width = 0;
        menu.menu[1][i].rectangle.height = 0;
        menu.menu[1][i].border.width = 0;
        menu.menu[1][i].border.height = 0;
    }
    

}

void init_option(menu_option_t *option, volatile color_t *lcd_option_start, char *text, 
font_descriptor_t font, uint8_t scaling,
color_t text_fill, color_t background_fill, color_t border_color, color_t rectangle_color)
{
    (*option).rectangle.lcd_rect_start = lcd_option_start + (strlen(text)-3) * font.maxwidth * scaling
     + WIDTH + 1;
    (*option).rectangle.color = rectangle_color;
    (*option).rectangle.width = (font.height - 2) * scaling;
    (*option).rectangle.height = (font.height - 2) * scaling;

    (*option).border.lcd_rect_start = lcd_option_start + (strlen(text)-3) * font.maxwidth * scaling;
    (*option).border.color = border_color;
    (*option).border.width = font.height * scaling;
    (*option).border.height = font.height * scaling;    

    (*option).text = text;
    (*option).text_fill = text_fill;
    (*option).background_fill = background_fill;
    (*option).lcd_option_start = lcd_option_start;   

    if (font.width != 0)
    {
        (*option).background.lcd_rect_start = lcd_option_start;
        (*option).background.width = string_lenght(OP_S2C_TEXT, font, scaling) + 4 + 16 * scaling + 4;
        (*option).background.height = font.height * scaling;
        (*option).background.color = background_fill;

        (*option).rectangle.lcd_rect_start = lcd_option_start + 
        string_lenght(OP_S2C_TEXT, font, scaling) + 4 + WIDTH + 1;
        (*option).border.lcd_rect_start = lcd_option_start + 
        string_lenght(OP_S2C_TEXT, font, scaling) + 4;
    }  
}

void print_menu(font_descriptor_t font)
{
    uint8_t lenght = menu.lenght;
    for (uint8_t i = 0; i < lenght; i++)
    {
        print_option(font, menu.menu[menu.menu_id][i]);        
    }    
}

void print_option(font_descriptor_t font, menu_option_t option)
{
    if (font.width != 0)
    {
        print_rectangle(option.background);
    }

    print_string(option.lcd_option_start, font, option.text,
        option.text_fill, option.background_fill, menu.scaling); 

    rectangle_t rectangle = option.border;
    print_rectangle(rectangle);

    rectangle = option.rectangle;
    print_rectangle(rectangle);
}

void print_rectangle(rectangle_t rectangle)
{
    volatile color_t *lcd_rect_start = rectangle.lcd_rect_start;

    for (uint16_t i = 0; i < rectangle.height; i++)
    {
        for (uint16_t j = 0; j < rectangle.width; j++)
        {
            *(lcd_rect_start + i * WIDTH + j) = rectangle.color;
        }        
    }    
}

void select_option(uint8_t option_index, font_descriptor_t font)
{
    color_t black = BLACK;
    color_t dark_blue = DARK_BLUE;

    if (font.width != 0)
    {
        menu.menu[menu.menu_id][menu.selected].background.color = black;
    }
    
    menu.menu[menu.menu_id][menu.selected].background_fill = black;
    print_option(font, menu.menu[menu.menu_id][menu.selected]);

    menu.selected = option_index;

    if (font.width != 0)
    {
        menu.menu[menu.menu_id][menu.selected].background.color = dark_blue;
    }    

    menu.menu[menu.menu_id][menu.selected].background_fill = dark_blue;
    print_option(font, menu.menu[menu.menu_id][menu.selected]);
}

void select_option_above(font_descriptor_t font)
{
    //uint8_t index = menu.selected == 0 ? menu.lenght-1 : menu.selected - 1;

    uint8_t index = menu.selected;
    if (index == 0)
    {
        index = menu.lenght-1;
    }
    else
    {
        index--;
    }

    select_option(index, font);
}

void select_option_below(font_descriptor_t font)
{
    //uint8_t index = menu.selected == menu.lenght - 1 ? 0 : menu.selected - 1;

    uint8_t index = menu.selected;
    if (index == menu.lenght - 1)
    {
        index = 0;
    }
    else
    {
        index++;
    }

    select_option(index, font);    
}

void process_selected_option(bool *menu_opened, bool *run, font_descriptor_t font,
game_t *game)
{
    color_t new_color;
    get_565_color_from_knobs(&new_color, game->io.spiled_reg_base);
    switch (menu.selected)
    {
    case 0:
        if (menu.menu_id == 0)
        {
            menu.menu_id = 1;
            print_menu(font);
        }
        else
        {
            menu.menu_id = 0; 
            *menu_opened = false;
            hide_menu(font);
            init_start(game);
            game->snake1.ai = false;
            game->snake2.ai = false;
        }       
        break; 
    case 1:
        if (menu.menu_id == 0)
        {
            menu.menu[menu.menu_id][menu.selected].rectangle.color = new_color;
            game->snake1.head_color = new_color;
            game->snake1.body_color = new_color;
            print_rectangle(menu.menu[menu.menu_id][menu.selected].rectangle);
        }
        else
        {
            menu.menu_id = 0; 
            *menu_opened = false;
            hide_menu(font);
            init_start(game);
            game->snake1.ai = false;
        }       
        break;    
    case 2:
        if (menu.menu_id == 0)
        {
            menu.menu[menu.menu_id][menu.selected].rectangle.color = new_color;
            game->snake2.head_color = new_color;
            game->snake2.body_color = new_color;
            print_rectangle(menu.menu[menu.menu_id][menu.selected].rectangle);
        }
        else
        {
            menu.menu_id = 0; 
            *menu_opened = false;
            hide_menu(font);
            init_start(game);
        }       
        break;    
    case 3:
        *run = false;
        break;    
    default:
        break;
    }
}

void create_speed_text(char* dest, const char *pre_speed, const char *post_speed, uint8_t speed)
{
    uint8_t max_lenght = strlen(dest);
    uint8_t cur_lenght = 0;
    uint8_t pre_speed_lenght = strlen(pre_speed);
    uint8_t post_speed_lenght = strlen(post_speed);

    for (uint8_t i = 0; i < pre_speed_lenght && max_lenght > cur_lenght; i++, cur_lenght++)
    {
        dest[cur_lenght] = pre_speed[i];   
    }

    dest[cur_lenght] = speed + 0x30;
    cur_lenght++;

    for (uint8_t i = 0; i < post_speed_lenght && max_lenght > cur_lenght; i++, cur_lenght++)
    {
        dest[cur_lenght] = post_speed[i];   
    }    
}

void hide_menu(font_descriptor_t font)
{
    uint8_t lenght = menu.lenght;
    color_t black = BLACK;
    for (uint8_t i = 0; i < lenght; i++)
    {
        if (font.width != 0)
        {
            color_t original_color = menu.menu[menu.menu_id][i].background.color;
            menu.menu[menu.menu_id][i].background.color = black;
            print_rectangle(menu.menu[menu.menu_id][i].background);
            menu.menu[menu.menu_id][i].background.color = original_color;
        }
        else
        {
            print_string(menu.menu[menu.menu_id][i].lcd_option_start, font, menu.menu[menu.menu_id][i].text,
                black, black, menu.scaling);
        }   
    } 
}
