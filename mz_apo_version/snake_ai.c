#include "snake_ai.h"
#include "stdio.h"

point_t find_next_move(snake_t snake1, snake_t snake2, game_t* game)
{
    int** food_path = malloc((WIDTH / SNAKE_SCALE + 1) * sizeof(int*));
    for(uint16_t i = 0; i < (WIDTH / SNAKE_SCALE + 1); i++)
    {
        food_path[i] = malloc((HEIGHT / SNAKE_SCALE + 1) * sizeof(int));
    }

    for(uint16_t i = 0; i < (WIDTH / SNAKE_SCALE); i++)
    {
        for(uint16_t j = 0; j < (HEIGHT / SNAKE_SCALE); j++)
        {
            food_path[i][j] = FREE_TILE;
        }
    }

    food_path[snake1.head.x / SNAKE_SCALE][snake1.head.y / SNAKE_SCALE] = OBSTACLE;
    if(snake1.body[snake1.body_lenght - 1].x + SNAKE_SCALE < WIDTH)
    {
    	food_path[(snake1.body[snake1.body_lenght - 1].x / SNAKE_SCALE) + 1][snake1.body[snake1.body_lenght - 1].y / SNAKE_SCALE] = OBSTACLE;
    }
    if(snake1.body[snake1.body_lenght - 1].x - SNAKE_SCALE >= 0)
    {
    	food_path[(snake1.body[snake1.body_lenght - 1].x / SNAKE_SCALE) - 1][snake1.body[snake1.body_lenght - 1].y / SNAKE_SCALE] = OBSTACLE;
    }
    if(snake1.body[snake1.body_lenght - 1].y + SNAKE_SCALE < HEIGHT)
    {
    	food_path[snake1.body[snake1.body_lenght - 1].x / SNAKE_SCALE][(snake1.body[snake1.body_lenght - 1].y / SNAKE_SCALE) + 1] = OBSTACLE;
    }
    if(snake1.body[snake1.body_lenght - 1].y - SNAKE_SCALE >= 0)
    {
    	food_path[snake1.body[snake1.body_lenght - 1].x / SNAKE_SCALE][(snake1.body[snake1.body_lenght - 1].y / SNAKE_SCALE) - 1] = OBSTACLE;
    }
    for(uint16_t k = 0; k < snake1.body_lenght; k++)
    {
       food_path[snake1.body[k].x / SNAKE_SCALE][snake1.body[k].y / SNAKE_SCALE] = OBSTACLE;
    }

    if (!snake2.stopped)
    {
        food_path[snake2.head.x / SNAKE_SCALE][snake2.head.y / SNAKE_SCALE] = OBSTACLE;
        if(snake2.head.x + SNAKE_SCALE < WIDTH) food_path[(snake2.head.x / SNAKE_SCALE) + 1][snake2.head.y / SNAKE_SCALE] = OBSTACLE;
        if(snake2.head.x - SNAKE_SCALE >= 0) food_path[(snake2.head.x / SNAKE_SCALE) - 1][snake2.head.y / SNAKE_SCALE] = OBSTACLE;
        if(snake2.head.y + SNAKE_SCALE < HEIGHT) food_path[snake2.head.x / SNAKE_SCALE][(snake2.head.y / SNAKE_SCALE) + 1] = OBSTACLE;
        if(snake2.head.y - SNAKE_SCALE >= 0) food_path[snake2.head.x / SNAKE_SCALE][(snake2.head.y / SNAKE_SCALE) - 1] = OBSTACLE;
        for(uint16_t k = 0; k < snake2.body_lenght; k++)
        {
            food_path[snake2.body[k].x / SNAKE_SCALE][snake2.body[k].y / SNAKE_SCALE] = OBSTACLE;
        }
    }    

    food_path[snake1.head.x / SNAKE_SCALE][snake1.head.y / SNAKE_SCALE] = 0;
    int pass = 0;
    int nodes_found = 1;
    if (food_path[game->food.x / SNAKE_SCALE][game->food.y / SNAKE_SCALE] == OBSTACLE)
    {
        nodes_found = 0;
    }    
    while (food_path[game->food.x / SNAKE_SCALE][game->food.y / SNAKE_SCALE] == FREE_TILE && nodes_found > 0)
    {
        nodes_found = 0;
        for(uint16_t i = 0; i < WIDTH / SNAKE_SCALE; i++)
        {
            for(uint16_t j = 0; j < HEIGHT / SNAKE_SCALE; j++)
            {
                if(food_path[i][j] == pass)
                {
                    nodes_found++;
                    set_neighbours(food_path, i, j);
                }
            }
        }
        pass++;
    }

    if(nodes_found > 0) return bactrack(food_path, game, game->food);
    else return bactrack(food_path, game, get_max_node(food_path));;
}

point_t bactrack(int** food_path, game_t* game, point_t position)
{
    uint16_t i = position.x / SNAKE_SCALE;
    uint16_t j = position.y / SNAKE_SCALE;
    point_t next_move;
    while (food_path[i][j] != 0)
    {
        if(i + 1 != WIDTH / SNAKE_SCALE && food_path[i + 1][j] == food_path[i][j] - 1) i++;
        else if(i - 1 >= 0 && food_path[i - 1][j] == food_path[i][j] - 1) i--;
        else if(j + 1 != HEIGHT / SNAKE_SCALE && food_path[i][j + 1] == food_path[i][j] - 1)  j++;
        else if(j - 1 >= 0 && food_path[i][j - 1]  == food_path[i][j] - 1) j--;
        if(food_path[i][j] == 1)
        {
            next_move.x = i * SNAKE_SCALE;
            next_move.y = j * SNAKE_SCALE;
        }
    }

    for(uint16_t i = 0; i < (HEIGHT / SNAKE_SCALE); i++)
    {
        free(food_path[i]);
    }
    free(food_path);

    return next_move;
}

void set_neighbours(int** food_path, uint16_t i, uint16_t j)
{
    if(i + 1 < WIDTH / SNAKE_SCALE && food_path[i + 1][j] == FREE_TILE) food_path[i + 1][j] = food_path[i][j] + 1;
    if(i - 1 >= 0 && food_path[i - 1][j] == FREE_TILE) food_path[i - 1][j] = food_path[i][j] + 1;
    if(j + 1 < HEIGHT / SNAKE_SCALE && food_path[i][j + 1] == FREE_TILE) food_path[i][j + 1] = food_path[i][j] + 1;
    if(j - 1 >= 0 && food_path[i][j - 1] == FREE_TILE) food_path[i][j - 1] = food_path[i][j] + 1;
}

void set_snake_direction(snake_t* snake1, snake_t snake2, game_t* game)
{
    point_t next_move = find_next_move(*snake1, snake2, game);
    if(snake1->head.x + SNAKE_SCALE < WIDTH && snake1->head.x + SNAKE_SCALE == next_move.x) snake1->direction = RIGHT;
    else if(snake1->head.x - SNAKE_SCALE >= 0 && snake1->head.x - SNAKE_SCALE == next_move.x) snake1->direction = LEFT;
    else if(snake1->head.y + SNAKE_SCALE < HEIGHT && snake1->head.y + SNAKE_SCALE == next_move.y) snake1->direction = DOWN;
    else if(snake1->head.y - SNAKE_SCALE >= 0 && snake1->head.y - SNAKE_SCALE == next_move.y) snake1->direction = UP;
}

point_t get_max_node(int** array)
{
	point_t point;
	int max = 0;
	int max_x = 0;
	int max_y = 0;
	
	for(uint16_t i = 0; i < WIDTH / SNAKE_SCALE; i++)
    {
        for(uint16_t j = 0; j < HEIGHT / SNAKE_SCALE; j++)
        {
            if(array[i][j] >= max)
            {
            	max = array[i][j];
            	max_x = i;
            	max_y = j;
            }
        }
    }
    
    point.x = max_x * SNAKE_SCALE;
    point.y = max_y * SNAKE_SCALE;
    return point;
}

