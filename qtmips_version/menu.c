#include "menu.h"
#include "snake_utils.h"

menu_t menu;

void print_char(volatile color_t *lcd_char_start, font_descriptor_t font,
char c, color_t text_fill, color_t background_fill, uint8_t scaling)
{
    int height_offset = 0; 
    for (int i = 0; i < font.height; i++)
    {
        uint16_t value = font.bits[(c - font.firstchar) * font.height + i];
        int width;
        if (font.width != 0)
        {
            width = font.width[c - font.firstchar];
        }
        else
        {
            width = font.maxwidth;
        }
        int width_stop = 16 - width;     
        
        for (int h_scale = 0; h_scale < scaling; h_scale++)
        {
            int width_offset = 0;
            for (int j = 15; j >= width_stop; j--)
            {
                for (int w_scale = 0; w_scale < scaling; w_scale++)
                {
                    if ((value>>j)&0b1)
                    {
                        *(lcd_char_start + height_offset + width_offset) = text_fill;
                    }
                    else
                    {
                        *(lcd_char_start + height_offset + width_offset) = background_fill;
                    }
                    width_offset++;
                }
            }
            height_offset += WIDTH;
        }
    }
}

void print_string(volatile color_t *lcd_string_start, font_descriptor_t font,
char* string, color_t text_fill, color_t background_fill, uint8_t scaling)
{
    int row_offset = 0;
    int char_offset = 0;
    while (*string != '\0')
    {
        print_char(lcd_string_start + row_offset + char_offset, font,
        *string, text_fill, background_fill, scaling);

        if (font.width != 0)
        {
            char_offset += font.width[(*string) - font.firstchar] * scaling;
        }
        else
        {
            char_offset += font.maxwidth * scaling;
        }

        if (char_offset > WIDTH - font.maxwidth * scaling)
        {
            char_offset = 0;
            row_offset += WIDTH * font.height * scaling;
        }
        string++;
    }
}

void init_menu(volatile color_t* lcd_start, font_descriptor_t font, 
snake_t *snake1, snake_t *snake2, uint8_t scaling)
{
    color_t white = WHITE;
    color_t gray = GRAY;
    color_t black = BLACK;
    color_t red = RED;
    color_t blue = BLUE;
    color_t magenta = MAGENTA;
    color_t violet = VIOLET;

    menu.lenght = 6;
    menu.selected = 0;
    menu.scaling = scaling;

    volatile color_t *lcd_resume_start = lcd_start + (HEIGHT - menu.lenght * font.height * scaling)/2
    * WIDTH + (WIDTH - (string_lenght(OP_S2HC_TEXT, font, scaling) + 24))/2;

    init_option(menu.menu, lcd_resume_start, OP_R_TEXT, font, scaling, red, gray, black, white);
    menu.menu[0].rectangle.width = 0;
    menu.menu[0].rectangle.height = 0;

    menu.menu[0].border.width = 0;
    menu.menu[0].border.height = 0;

    volatile color_t *lcd_s1hc_start = lcd_resume_start + font.height * WIDTH * scaling;
    
    snake1->head_color = blue;
    init_option(menu.menu + 1, lcd_s1hc_start, OP_S1HC_TEXT, font, scaling, red, white, black, blue);
 
    volatile color_t *lcd_s1bc_start = lcd_s1hc_start + font.height * WIDTH * scaling;

    snake1->body_color = violet;
    init_option(menu.menu + 2, lcd_s1bc_start, OP_S1BC_TEXT, font, scaling, red, white, black, violet);

    volatile color_t *lcd_s2hc_start = lcd_s1bc_start + font.height * WIDTH * scaling;

    snake2->head_color = red;
    init_option(menu.menu + 3, lcd_s2hc_start, OP_S2HC_TEXT, font, scaling, red, white, black, red);
 
    volatile color_t *lcd_s2bc_start = lcd_s2hc_start + font.height * WIDTH * scaling;

    snake2->body_color = magenta;
    init_option(menu.menu + 4, lcd_s2bc_start, OP_S2BC_TEXT, font, scaling, red, white, black, magenta);

    /*volatile color_t *lcd_speed_start = lcd_s2bc_start + font.height * WIDTH;

    init_option(menu.menu + 5, lcd_speed_start, OP_SPEED_TEXT, font, red, white, black, white);
    menu.menu[5].rectangle.width = 0;
    menu.menu[5].rectangle.height = 0;

    menu.menu[5].border.width = 0;
    menu.menu[5].border.height = 0;*/

    volatile color_t *lcd_exit_start = lcd_s2bc_start + font.height * WIDTH * scaling;

    init_option(menu.menu + 5, lcd_exit_start, OP_EXIT_TEXT, font, scaling, red, white, black, white);
    menu.menu[5].rectangle.width = 0;
    menu.menu[5].rectangle.height = 0;

    menu.menu[5].border.width = 0;
    menu.menu[5].border.height = 0;

    if (font.width != 0)
    {
        menu.menu[0].background.lcd_rect_start = lcd_resume_start;
        menu.menu[0].background.width = string_lenght(OP_S2HC_TEXT, font, scaling) + 4 + 16 * scaling + 4;
        menu.menu[0].background.height = font.height * scaling;
        menu.menu[0].background.color = menu.menu[0].background_fill;

        menu.menu[5].background.lcd_rect_start = lcd_exit_start;
        menu.menu[5].background.width = string_lenght(OP_S2HC_TEXT, font, scaling) + 4 + 16 * scaling + 4;
        menu.menu[5].background.height = font.height * scaling;
        menu.menu[5].background.color = menu.menu[5].background_fill;
    }
}

void init_option(menu_option_t *option, volatile color_t *lcd_option_start, char *text, 
font_descriptor_t font, uint8_t scaling,
color_t text_fill, color_t background_fill, color_t border_color, color_t rectangle_color)
{
    (*option).rectangle.lcd_rect_start = lcd_option_start + (strlen(text)-3) * font.maxwidth * scaling
     + WIDTH + 1;
    (*option).rectangle.color = rectangle_color;
    (*option).rectangle.width = (font.height - 2) * scaling;
    (*option).rectangle.height = (font.height - 2) * scaling;

    (*option).border.lcd_rect_start = lcd_option_start + (strlen(text)-3) * font.maxwidth * scaling;
    (*option).border.color = border_color;
    (*option).border.width = font.height * scaling;
    (*option).border.height = font.height * scaling;

    (*option).text = text;
    (*option).text_fill = text_fill;
    (*option).background_fill = background_fill;
    (*option).lcd_option_start = lcd_option_start;

    if (font.width != 0)
    {
        (*option).background.lcd_rect_start = lcd_option_start;
        (*option).background.width = string_lenght(OP_S2HC_TEXT, font, scaling) + 4 + 16 * scaling + 4;
        (*option).background.height = font.height * scaling;
        (*option).background.color = background_fill;

        (*option).rectangle.lcd_rect_start = lcd_option_start + 
        string_lenght(OP_S2HC_TEXT, font, scaling) + 4 + WIDTH + 1;
        (*option).border.lcd_rect_start = lcd_option_start + 
        string_lenght(OP_S2HC_TEXT, font, scaling) + 4;
    }   
}

void print_menu(font_descriptor_t font)
{
    uint8_t lenght = menu.lenght;
    for (uint8_t i = 0; i < lenght; i++)
    {
        print_option(font, menu.menu[i]);        
    }    
}

void print_option(font_descriptor_t font, menu_option_t option)
{
    if (font.width != 0)
    {
        print_rectangle(option.background);
    }    

    print_string(option.lcd_option_start, font, option.text,
        option.text_fill, option.background_fill, menu.scaling); 

    rectangle_t rectangle = option.border;
    print_rectangle(rectangle);

    rectangle = option.rectangle;
    print_rectangle(rectangle);
}

void print_rectangle(rectangle_t rectangle)
{
    volatile color_t *lcd_rect_start = rectangle.lcd_rect_start;

    for (uint16_t i = 0; i < rectangle.height; i++)
    {
        for (uint16_t j = 0; j < rectangle.width; j++)
        {
            *(lcd_rect_start + i * WIDTH + j) = rectangle.color;
        }        
    }    
}

void select_option(uint8_t option_index, font_descriptor_t font)
{
    color_t white = WHITE;
    color_t gray = GRAY;

    if (font.width != 0)
    {
        menu.menu[menu.selected].background.color = white;
    }
    
    menu.menu[menu.selected].background_fill = white;
    print_option(font, menu.menu[menu.selected]);

    menu.selected = option_index;

    if (font.width != 0)
    {
        menu.menu[menu.selected].background.color = gray;
    }    

    menu.menu[menu.selected].background_fill = gray;
    print_option(font, menu.menu[menu.selected]);
}

void select_option_above(font_descriptor_t font)
{
    //uint8_t index = menu.selected == 0 ? menu.lenght-1 : menu.selected - 1;

    uint8_t index = menu.selected;
    if (index == 0)
    {
        index = menu.lenght-1;
    }
    else
    {
        index--;
    }

    select_option(index, font);
}

void select_option_below(font_descriptor_t font)
{
    //uint8_t index = menu.selected == menu.lenght - 1 ? 0 : menu.selected - 1;

    uint8_t index = menu.selected;
    if (index == menu.lenght - 1)
    {
        index = 0;
    }
    else
    {
        index++;
    }

    select_option(index, font);    
}

void process_selected_option(bool *menu_opened, bool *run, font_descriptor_t font,
game_t *game)
{
    color_t new_color;
    get_565_color_from_knobs(&new_color, (uint8_t*)SPILED_REG_BASE);
    char new_speed[strlen(OP_SPEED_PRE) + strlen(OP_SPEED_POST) + 2];
    char speed_char;
    uint8_t speed;
    switch (menu.selected)
    {
    case 0:
        *menu_opened = false;
        hide_menu(font);
        init_start(game);
        break; 
    case 1:
        menu.menu[menu.selected].rectangle.color = new_color;
        game->snake1.head_color = new_color;
        print_rectangle(menu.menu[menu.selected].rectangle);
        break;    
    case 2:
        menu.menu[menu.selected].rectangle.color = new_color;
        game->snake1.body_color = new_color;
        print_rectangle(menu.menu[menu.selected].rectangle);
        break;    
    case 3:
        menu.menu[menu.selected].rectangle.color = new_color;
        game->snake2.head_color = new_color;
        print_rectangle(menu.menu[menu.selected].rectangle);
        break;    
    case 4:
        menu.menu[menu.selected].rectangle.color = new_color;
        game->snake2.body_color = new_color;
        print_rectangle(menu.menu[menu.selected].rectangle);
        break;    
    case 5:
        read_char_stop_and_wait(game->io.serial_port_base, &speed_char);
        if (speed_char >= 0x30 && speed_char <=0x39)
        {
            game->game_speed = (uint8_t)speed_char - 0x30;
        }        
        create_speed_text(menu.menu[menu.selected].text, OP_SPEED_PRE, OP_SPEED_POST, game->game_speed);
        print_option(font, menu.menu[menu.selected]);
        break;
    case 6:
        *run = false;
        break;    
    default:
        break;
    }
}

void create_speed_text(char* dest, const char *pre_speed, const char *post_speed, uint8_t speed)
{
    uint8_t max_lenght = strlen(dest);
    uint8_t cur_lenght = 0;
    uint8_t pre_speed_lenght = strlen(pre_speed);
    uint8_t post_speed_lenght = strlen(post_speed);

    for (uint8_t i = 0; i < pre_speed_lenght && max_lenght > cur_lenght; i++, cur_lenght++)
    {
        dest[cur_lenght] = pre_speed[i];   
    }

    dest[cur_lenght] = speed + 0x30;
    cur_lenght++;

    for (uint8_t i = 0; i < post_speed_lenght && max_lenght > cur_lenght; i++, cur_lenght++)
    {
        dest[cur_lenght] = post_speed[i];   
    }    
}

void hide_menu(font_descriptor_t font)
{
    uint8_t lenght = menu.lenght;
    color_t white = WHITE;
    for (uint8_t i = 0; i < lenght; i++)
    {
        if (font.width != 0)
        {
            color_t original_color = menu.menu[i].background.color;
            menu.menu[i].background.color = white;
            print_rectangle(menu.menu[i].background);
            menu.menu[i].background.color = original_color;
        }
        else
        {
            print_string(menu.menu[i].lcd_option_start, font, menu.menu[i].text,
                white, white, menu.scaling);
        }
    } 
}
