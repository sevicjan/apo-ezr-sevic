#ifndef SNAKE_UTILS_H
#define SNAKE_UTILS_H
#define UP 1
#define DOWN 2
#define LEFT 3
#define RIGHT 4
#define INIT_SNAKE_LENGHT 5
#define WIN_SCORE 25

#include "color.h"
#include "font_types.h"
#include <stdbool.h>
#include "qtmips_peripherials.h"
#include "utils.h"
#include "snake.h"

void generate_food(game_t* game);

void init_snake(snake_t* snake, point_t head_pos, uint16_t b_lenght, int direction);

bool food_eaten(snake_t*, point_t food);

void move_snake(snake_t* snake, point_t food);

void update_snakes(game_t* game);

void draw_snake_rectangle(game_t* game, point_t position, color_t color);

bool snake_collision(snake_t snake);

bool snakes_collided(snake_t snake1, snake_t snake2);

void delete_snake(snake_t* snake, game_t* game);

#endif