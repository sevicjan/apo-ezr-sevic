#include "snake_utils.h"
#include "utils.h"
#include <stdbool.h>
//480 x 320

void generate_food(game_t* game)
{
    point_t food;
    bool loop;
    while(true)
    {
        loop = false;
        food.x = (get_rand(game) % 240) * 2;
        food.y = (get_rand(game) % 160) * 2;
        if(food.x == game->snake1.head.x && food.y ==  game->snake1.head.y)
        {
            continue;
        }
        else if(food.x ==  game->snake2.head.x && food.y ==  game->snake2.head.y)
        {
            continue;
        }
        for(int i = 0; i <  game->snake1.body_lenght; i++)
        {
            if( game->snake1.body[i].x == food.x &&  game->snake1.body[i].y == food.y)
            {
                loop = true;
                break;
            }
        }
        for(int i = 0; i <  game->snake2.body_lenght; i++)
        {
            if( game->snake2.body[i].x == food.x &&  game->snake2.body[i].y == food.y)
            {
                loop = true;
                break;
            }
        }
        if(loop)
        {
            continue;
        }
        break;
    }

    game->food.x = food.x;
    game->food.y = food.y;
}

void init_snake(snake_t* snake, point_t head_pos, uint16_t b_lenght, int direction)
{
    snake->head = head_pos;
    snake->body_lenght = b_lenght;
    snake->stopped = false;
    for(int i = 1; i <= b_lenght; i++)
    {
        if(direction == UP) 
        {
            snake->body[i-1].x = snake->head.x; 
            snake->body[i-1].y = snake->head.y - (i * 2);
        }      
        else if(direction == DOWN)
        {
            snake->body[i-1].x = snake->head.x; 
            snake->body[i-1].y = snake->head.y + (i * 2);
        }
        else if(direction == LEFT)
        {
            snake->body[i-1].x = snake->head.x - (i * 2); 
            snake->body[i-1].y = snake->head.y;
        }
        else if(direction == RIGHT)
        {
            snake->body[i-1].x = snake->head.x + (i * 2); 
            snake->body[i-1].y = snake->head.y;
        }
    }
}

bool food_eaten(snake_t* snake, point_t food)
{
    if(snake->head.x == food.x && snake->head.y == food.y)
    {
        snake->body_lenght += 1;
        return true;
    }
    else return false;
}

void move_snake(snake_t* snake, point_t food)
{   
    point_t body_buffer[2];
    body_buffer[0].x = snake->head.x;
    body_buffer[0].y = snake->head.y;

    for(uint16_t i = 0; i < snake->body_lenght-1; i++)
    {
        body_buffer[(i + 1) & 1].x = snake->body[i].x;
        body_buffer[(i + 1) & 1].y = snake->body[i].y;

        snake->body[i].x = body_buffer[i & 1].x;
        snake->body[i].y = body_buffer[i&1].y;
    }
    snake->body[snake->body_lenght - 1].x = body_buffer[(snake->body_lenght - 1) & 1].x;
    snake->body[snake->body_lenght - 1].y = body_buffer[(snake->body_lenght - 1) & 1].y;
    

    /*point_t body_buffer[100];
    for(uint16_t i = 0; i < snake->body_lenght; i++)
    {
        body_buffer[i] = snake->body[i];
    }
    for(uint16_t i = 1; i < snake->body_lenght; i++)
    {
        snake->body[i] = body_buffer[i - 1];
    }
    snake->body[0].x = snake->head.x;
    snake->body[0].y = snake->head.y;*/

    if(snake->direction == UP) snake->head.y -= 2;          
    else if(snake->direction == DOWN) snake->head.y += 2;
    else if(snake->direction == LEFT) snake->head.x -= 2;
    else if(snake->direction == RIGHT) snake->head.x += 2;   
}

void update_snakes(game_t* game)
{
    point_t gen_food;
    color_t white = WHITE;
    color_t black = BLACK;
    if(!game->snake1.stopped)
    {
        
        if(!food_eaten(&(game->snake1), game->food))
        {
            draw_snake_rectangle(game, game->snake1.body[game->snake1.body_lenght-1], white);
        }
        else 
        {
            generate_food(game);
            draw_snake_rectangle(game, game->food, black);
        } 
        move_snake(&(game->snake1), game->food);
        draw_snake_rectangle(game, game->snake1.head, game->snake1.head_color);
        draw_snake_rectangle(game, game->snake1.body[0], game->snake1.body_color);
    }
    
    if(!game->snake2.stopped)
    {
        if(!food_eaten(&(game->snake2), game->food))
        {
            draw_snake_rectangle(game, game->snake2.body[game->snake2.body_lenght-1], white);
        }
        else 
        {
            generate_food(game);
            draw_snake_rectangle(game, game->food, black);
        } 
        move_snake(&(game->snake2), game->food);   
        draw_snake_rectangle(game, game->snake2.head, game->snake2.head_color); 
        draw_snake_rectangle(game, game->snake2.body[0], game->snake2.body_color);
    }
}

void draw_snake_rectangle(game_t* game, point_t position, color_t color)
{
    game->io.lcd.lcd_start[position.y * WIDTH + position.x] = color;
    game->io.lcd.lcd_start[(position.y + 1) * WIDTH + position.x] = color;
    game->io.lcd.lcd_start[position.y * WIDTH + (position.x + 1)] = color;
    game->io.lcd.lcd_start[(position.y + 1) * WIDTH + (position.x + 1)] = color;
}

bool snake_collision(snake_t snake)
{
    if(!snake.stopped)
    {
        if(snake.head.x > WIDTH || snake.head.x < 0 || snake.head.y > HEIGHT || snake.head.y < 0)
        {
            return true;
        }
        for(int i = 0; i < snake.body_lenght; i++)
        {
            if(snake.head.x == snake.body[i].x && snake.head.y == snake.body[i].y) return true;
        }
    }
    return false;
}

bool snakes_collided(snake_t snake1, snake_t snake2)
{
    if(!snake1.stopped && !snake2.stopped)
    {
        if(snake1.head.x == snake2.head.x && snake1.head.y == snake2.head.y) return true;
        for(int i = 0; i < snake2.body_lenght; i++)
        {
            if(snake1.head.x == snake2.body[i].x && snake1.head.y == snake2.body[i].y) return true;
        }
        for(int i = 0; i < snake1.body_lenght; i++)
        {
            if(snake2.head.x == snake1.body[i].x && snake2.head.y == snake1.body[i].y) return true;
        }
    }   
    return false;
}

void delete_snake(snake_t* snake, game_t* game)
{
    color_t white = WHITE;
    draw_snake_rectangle(game, snake->head, white);
    for(uint16_t i = 0; i < snake->body_lenght; i++)
    {
        draw_snake_rectangle(game, snake->body[i], white);
    }
}
