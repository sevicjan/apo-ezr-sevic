#ifndef SNAKE_H
#define SNAKE_H

#include "color.h"
#include <stdbool.h>

typedef struct
{
    uint16_t x;
    uint16_t y;
} point_t;

typedef struct
{
    point_t head;
    point_t body[100];
    color_t head_color;
    color_t body_color;
    uint16_t body_lenght;
    int direction;
    bool stopped;
} snake_t;



#endif
