#define _POSIX_C_SOURCE 200112L

#include "snake_utils.h"
#include "menu.h"
#include "font_rom8x16.c"
#include "font_prop14x16.c"
//480 x 320

game_t game;

int _start(int argc, char ** argv)
{
    volatile color_t *lcd_start = (color_t*)LCD_FB_START;

    color_t white = WHITE;

    init_game(white, &game);
    init_menu(lcd_start, font_winFreeSystem14x16, &(game.snake1), &(game.snake2), 1);
    print_menu(font_winFreeSystem14x16);

    bool run = true;
    bool menu_open = true;
    while (run)
    {
        process_terminal_input(&game, &menu_open, &run, font_winFreeSystem14x16);
        if(!menu_open) 
        {
            update_snakes(&game);
            run = !check_win_conditions(&game);
        }
    }
        
    return 0;
}