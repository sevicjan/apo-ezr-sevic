#include "utils.h"
#include "snake_utils.h"
#include "menu.h"

unsigned get_rand(game_t* game)
{
    unsigned bit  = ((game->seed >> 0) ^ (game->seed >> 2) ^ (game->seed >> 3) ^ (game->seed >> 5) ) & 1;
    return game->seed = (game->seed >> 1) | (bit << 15);
}

size_t strlen(const char* string)
{
    size_t lenght = 0;
    while (*string != '\0')
    {
        lenght++;
        string++;
    }    
    return lenght;
}

uint32_t string_lenght(const char *string, font_descriptor_t font, uint8_t scaling)
{
    uint32_t lenght = 0;
    if (font.width != 0)
    {
        while (*string != '\0')
        {
            lenght += font.width[(*string) - font.firstchar];
            string++;
        }
    }
    else
    {
        lenght = strlen(string) * font.maxwidth;
    }
    return lenght * scaling;
}

void get_565_color_from_knobs(color_t *color_565, volatile uint8_t *spiled_reg_base)
{
    volatile knobs_color_t *spiled_knobs = (knobs_color_t*)(spiled_reg_base + SPILED_REG_KNOBS_8BIT_o);
    uint8_t red = spiled_knobs->red;
    uint8_t green = spiled_knobs->green;
    uint8_t blue = spiled_knobs->blue;

    color_565->_rgb.red = red>>3;
    color_565->_rgb.green = green>>2;
    color_565->_rgb.blue = blue>>3;
}

void write_knobs_to_leds(volatile uint8_t *spiled_reg_base)
{
    volatile knobs_color_t *spiled_knobs = (knobs_color_t*)(spiled_reg_base + SPILED_REG_KNOBS_8BIT_o);
    volatile knobs_color_t *spiled_led1 = (knobs_color_t*)(spiled_reg_base + SPILED_REG_LED_RGB1_o);
    volatile knobs_color_t *spiled_led2 = (knobs_color_t*)(spiled_reg_base + SPILED_REG_LED_RGB2_o);
    *spiled_led1 = *spiled_knobs;
    *spiled_led2 = *spiled_knobs;
}

void init_lcd(color_t initial_color, lcd_t *lcd)
{
    volatile color_t *lcd_start = (color_t*)LCD_FB_START;

    lcd->height = HEIGHT;
    lcd->width = WIDTH;
    lcd->size = HEIGHT * WIDTH;
    lcd->lcd_start = lcd_start;

    for (uint32_t i = 0; i < lcd->size; i++)
    {
        *(lcd_start + i) = initial_color;
    }
}

void init_io(color_t initial_color, io_t *io)
{
    init_lcd(initial_color, &(io->lcd));
    io->serial_port_base = (uint8_t*)(SERIAL_PORT_BASE);
    io->spiled_reg_base = (uint8_t*)(SPILED_REG_BASE);
}

void read_char(volatile uint8_t *serial_port_base, char *c)
{
    volatile uint32_t *rx_st_reg = (uint32_t*)(serial_port_base + SERP_RX_ST_REG_o);
    volatile uint32_t *rx_data_reg = (uint32_t*)(serial_port_base + SERP_RX_DATA_REG_o);

    if (*rx_st_reg & SERP_RX_ST_REG_READY_m)
    {
        *c = (char)(*rx_data_reg);
    }
    else
    {
        *c = '\0';
    }       
}

void read_char_stop_and_wait(volatile uint8_t *serial_port_base, char *c)
{
    volatile uint32_t *rx_st_reg = (uint32_t*)(serial_port_base + SERP_RX_ST_REG_o);
    volatile uint32_t *rx_data_reg = (uint32_t*)(serial_port_base + SERP_RX_DATA_REG_o);

    while (true)
    {
        if (*rx_st_reg & SERP_RX_ST_REG_READY_m)
        {
            *c = (char)(*rx_data_reg);
            break;
        }  
    }       
}

void process_terminal_input(game_t *game, bool *menu_open, bool *run, font_descriptor_t font)
{
    char user_input;
    read_char(game->io.serial_port_base, &user_input);

    write_knobs_to_leds(game->io.spiled_reg_base);

    switch (user_input)
    {
    case 'e':
    case 'E':        
        if (*menu_open)
        {
            process_selected_option(menu_open, run, font, game);               
        }
        else
        {
            *menu_open = true;
            print_menu(font);
        }
        break;
    case 'w':
    case 'W':
        if (*menu_open)
        {
            select_option_above(font);                
        }
        else
        {
            //Set snake1 direction up
            if(game->snake1.direction != DOWN) game->snake1.direction = UP;
        }
        break;
    case 's':
    case 'S':
        if (*menu_open)
        {
            select_option_below(font);            
        }
        else
        {
            //Set snake1 direction left
            if(game->snake1.direction != UP) game->snake1.direction = DOWN;
        }            
        break;
    case 'a':
    case 'A':
        if (!(*menu_open))
        {
            //Set snake1 direction left    
            if(game->snake1.direction != RIGHT) game->snake1.direction = LEFT;             
        }       
        break;  
    case 'd':
    case 'D':
        if (!(*menu_open))
        {
            //Set snake1 direction right 
            if(game->snake1.direction != LEFT) game->snake1.direction = RIGHT;             
        }
        break;
    case 'i':
    case 'I':
        if (*menu_open)
        {
            select_option_above(font);                
        }
        else
        {
            //Set snake2 direction up   
            if(game->snake2.direction != DOWN) game->snake2.direction = UP;
        }            
        break;  
    case 'k':
    case 'K':
        if (*menu_open)
        {
            select_option_below(font);            
        }
        else
        {
            //Set snake2 direction left
            if(game->snake2.direction != UP) game->snake2.direction = DOWN;
        }            
        break;
    case 'j':
    case 'J':
        if (!(*menu_open))
        {
            //Set snake2 direction left   
            if(game->snake2.direction != RIGHT)game->snake2.direction = LEFT;               
        }       
        break;  
    case 'l':
    case 'L':
        if (!(*menu_open))
        {
            //Set snake2 direction right  
            if(game->snake2.direction != LEFT)game->snake2.direction = RIGHT;            
        }
        break;     
    default:
        break;
    }
}

int strcmp(const char* text1, const char* text2)
{
    while (*text1 != '\0' || *text2 != '\0')
    {
        if(*text1 != *text2) return -1;
        text1++;
        text2++;
    }
    return 0;
}

void init_game(color_t initial_color, game_t *game)
{
    init_io(initial_color, &(game->io));
    game->game_speed = 1;
    game->font_size = 1;
    game->seed = 0xACE1u;
}

void init_start(game_t* game)
{
    point_t pos1;
    pos1.x = 20;
    pos1.y = 20;

    point_t pos2;
    pos2.x = 460;
    pos2.y = 300;
    
    generate_food(game);

    color_t black = BLACK;
    color_t white = WHITE;
    init_snake(&(game->snake1), pos1, INIT_SNAKE_LENGHT, UP);
    init_snake(&(game->snake2), pos2, INIT_SNAKE_LENGHT, DOWN);
    game->snake1.direction = DOWN;
    game->snake2.direction = UP;
    draw_snake_rectangle(game, game->snake1.head, game->snake1.head_color);
    for(int i = 0; i <  game->snake1.body_lenght; i++)
    {
        draw_snake_rectangle(game, game->snake1.body[i], game->snake1.body_color);
    }
    draw_snake_rectangle(game, game->snake2.head, game->snake2.head_color);
    for(int i = 0; i <  game->snake2.body_lenght; i++)
    {
        draw_snake_rectangle(game, game->snake2.body[i], game->snake2.body_color);
    }
    draw_snake_rectangle(game, game->food, black);
}

bool check_win_conditions(game_t* game)
{
    if(snakes_collided(game->snake1, game->snake2))
    {
        //Print draw
        delete_snake(&(game->snake1), game);
        delete_snake(&(game->snake2), game);
        print_text_to_serp(&game->io, "Game ended in a draw!\n");
        return true;
    }
    else if(game->snake1.stopped && game->snake2.stopped && game->snake1.body_lenght == game->snake2.body_lenght)
    {
        //Print draw
        delete_snake(&(game->snake1), game);
        delete_snake(&(game->snake2), game);
        print_text_to_serp(&game->io, "Game ended in a draw!\n");
        return true;
    }
    else if(game->snake1.body_lenght == WIN_SCORE)
    {
        //Print snake 1 wins
        delete_snake(&(game->snake1), game);
        delete_snake(&(game->snake2), game);
        return true;
    }
    else if(game->snake2.body_lenght == WIN_SCORE)
    {
        //Print snake 2 wins
        delete_snake(&(game->snake1), game);
        delete_snake(&(game->snake2), game);
        return true;
    }
    else if(game->snake1.stopped && game->snake2.body_lenght > game->snake1.body_lenght)
    {
        //Print snake 2 wins
        delete_snake(&(game->snake1), game);
        delete_snake(&(game->snake2), game);
        if(game->snake1.body_lenght == WIN_SCORE && game->snake2.body_lenght == WIN_SCORE)
        {
            print_text_to_serp(&game->io, "Game ended in a draw!\n");
            return true;
        }
        else if(game->snake1.body_lenght == WIN_SCORE)
        {
            print_text_to_serp(&game->io, "Snake1 wins the game!\n");
            return true;
        }
        else if(game->snake2.body_lenght == WIN_SCORE)
        {
            print_text_to_serp(&game->io, "Snake2 wins the game!\n");
            return true;
        }
    }
    else if(game->snake1.stopped && game->snake2.body_lenght > game->snake1.body_lenght)
    {
        print_text_to_serp(&game->io, "Snake2 wins the game!\n");
        return true;
    }
    else if(game->snake2.stopped && game->snake1.body_lenght > game->snake2.body_lenght)
    {
        //Print snake 1 wins
        delete_snake(&(game->snake1), game);
        delete_snake(&(game->snake2), game);
        print_text_to_serp(&game->io, "Snake1 wins the game!\n");
        return true;
    }
    else if(snake_collision(game->snake1) && !game->snake1.stopped)
    {
        if(game->snake1.body_lenght < game->snake2.body_lenght)
        {
            //Print snake 2 wins
            delete_snake(&(game->snake1), game);
            delete_snake(&(game->snake2), game);
            print_text_to_serp(&game->io, "Snake2 wins the game!\n");
            return true;
        }
        else if(game->snake1.body_lenght >= game->snake2.body_lenght)
        {
            game->snake1.stopped = true;
            delete_snake(&(game->snake1), game);
        }
    }
    else if(snake_collision(game->snake2) && !game->snake2.stopped)
    {
        if(game->snake2.body_lenght < game->snake1.body_lenght)
        {
            //Print snake 1 wins
            delete_snake(&(game->snake1), game);
            delete_snake(&(game->snake2), game);
            print_text_to_serp(&game->io, "Snake1 wins the game!\n");
            return true;
        }
        else if(game->snake2.body_lenght >= game->snake1.body_lenght) 
        {
            game->snake2.stopped = true;
            delete_snake(&(game->snake2), game);
        }
    }
    return false;
}

void print_text_to_serp(io_t* io, char *text)
{
    while (*text != '\0')
    {
        if ((*((volatile uint32_t*)(io->serial_port_base + SERP_TX_ST_REG_o))) & SERP_TX_ST_REG_READY_m)
        {
            *((volatile uint32_t*)(io->serial_port_base + SERP_TX_DATA_REG_o)) = *text;
            text++;
        }           
    }    
}
