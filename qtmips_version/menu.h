#ifndef MENU_H
#define MENU_H

#include "font_types.h"
#include "snake.h"
#include "qtmips_peripherials.h"
#include "color.h"
#include "utils.h"

#define OP_R_TEXT     " start               "
#define OP_S1HC_TEXT  " snake1 head color   "
#define OP_S1BC_TEXT  " snake1 body color   "
#define OP_S2HC_TEXT  " snake2 head color   "
#define OP_S2BC_TEXT  " snake2 body color   "
#define OP_SPEED_TEXT " speed:1             "
#define OP_EXIT_TEXT  " exit application    "

#define OP_SPEED_PRE " speed:"
#define OP_SPEED_POST "          "

typedef struct menu_option
{
    char *text;
    color_t text_fill;
    color_t background_fill;
    volatile color_t *lcd_option_start;
    rectangle_t background;
    rectangle_t rectangle;
    rectangle_t border;
} menu_option_t;

typedef struct
{
    menu_option_t menu[6];
    uint8_t selected;
    uint8_t lenght;
    uint8_t scaling;
} menu_t;

void print_char(volatile color_t *lcd_char_start, font_descriptor_t font, 
char c, color_t text_fill, color_t background_fill, uint8_t scaling);

void print_string(volatile color_t *lcd_string_start, font_descriptor_t font,
char* string, color_t text_fill, color_t background_fill, uint8_t scaling);

void init_menu(volatile color_t *lcd_start, font_descriptor_t font,
snake_t *snake1, snake_t *snake2, uint8_t scaling);

void init_option(menu_option_t *option, volatile color_t *lcd_start, char *text, font_descriptor_t font,
uint8_t scaling,
color_t text_fill, color_t background_fill, color_t border_color, color_t rectangle_color);

void print_menu(font_descriptor_t font);

void print_option(font_descriptor_t font, menu_option_t option);

void print_rectangle(rectangle_t rectangle);

void select_option(uint8_t option_index, font_descriptor_t font);

void select_option_above(font_descriptor_t font);

void select_option_below(font_descriptor_t font);

void process_selected_option(bool *menu_opened, bool *run, font_descriptor_t font, 
game_t *game);

void create_speed_text(char* dest, const char *pre_speed, const char *post_speed, uint8_t speed);

void hide_menu(font_descriptor_t font);

#endif
